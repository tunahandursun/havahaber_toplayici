#!/usr/bin/python
# -*-coding:utf-8-*-
import ConfigParser
import feedparser
import psycopg2
import sys
import logging
import time
import socket

reload(sys)
sys.setdefaultencoding('utf8')
def tablo_olustur(): # Eger feedlerin eklenecegi tablo yoksa olusturuluyor
    cur = conn.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS %s
           (id SERIAL PRIMARY  KEY    NOT NULL,
           title            TEXT    NOT NULL,
           description          TEXT     NOT NULL,
           link             TEXT,
           pubdate          TEXT
           );''' %(tablo_adi))

def tabloya_ekle(site_adi): # Parametre aldigi site ismine baglanir ve rss verilerini alir, tabloya ekler
    cur = conn.cursor()
    myfeed = feedparser.parse(site_adi)
    kayitci.info("%s okunuyor." %(site_adi))
    if len(myfeed.entries) != 0: # Eger site kullanilmiyor veya bi sorun varsa myfeed.entries bos liste oluyor. Bos olmasi durumunda fonksyondan cikiliyor.
        for a in myfeed.entries:
            try:
                cur.execute("SELECT title FROM haber WHERE title=(%s);",(a.title,))
                temp = cur.fetchone()

                if temp is None: # Veritabanina daha önce eklenip eklenmedigi kontrol ediliyor
                    cur.execute("INSERT INTO haber(title, description, link, pubdate) VALUES(%s,%s,%s,%s)",(a.title,a.summary,a.link,a.published))
                    conn.commit()
                else:
                    return # Eger temp none degilse daha once kayit eklenmistir ve veriler tarih sirali geldiginden sonrakileri kontrol etmeye gerek yoktur (?)
            except KeyboardInterrupt:
                kapat()
            except TypeError as exp:
                kayitci.info("tabloya_ekle fonksyonunda tip hatasi. <--> %s" %(str(exp)))
                kapat()
            except Exception as exp:
                kayitci.info("tabloya_ekle fonksyonunda bilinmeyen hata <--> %s" %(str(exp)))
                kapat()
    else:
        kayitci.warning("Link hatali veya kullanilamiyor, atlaniyor.")
def kapat():
        conn.close()
        quit()
try: # Log ve config dosyalari aciliyor.
    dosya = logging.FileHandler("lololo.log")
    dosya.setFormatter(logging.Formatter('%(asctime)s - %(name)s [%(levelname)s]: %(message)s'))
    kayitci = logging.getLogger("ana-modul")
    kayitci.setLevel(logging.DEBUG)
    kayitci.addHandler(dosya)
    try:
        kayitci.info('Konfigurasyon dosyasi okunuyor')
        config = ConfigParser.RawConfigParser()
        config.read('havahaber.cfg')
        port = config.getint('Database','port')
        kullanici_adi = config.get('Database','kullanici_adi')
        kullanici_parola = config.get('Database','kullanici_parola')
        host = config.get('Database','host')
        tablo_adi = config.get('Database','tablo_adi')
        site_timeout = config.getint('Genel','site_timeout')
        periyot = config.getint('Genel','periyot')
    except Exception as exp:
        kayitci.error("Konfigurasyon dosyasinda sorun var. %s" %(exp))
        quit()
    socket.setdefaulttimeout(site_timeout) # Siteye baglanirken time out suresi belirleniyor. (konf dosyasindan okunuyor)

    kayitci.info("Database baglantisi kuruluyor")
    conn = psycopg2.connect(database="havahaber", user=kullanici_adi, password=kullanici_parola, host=host, port=port)
    kayitci.info("Database baglantisi kuruldu, \"haber\" tablosunun varligi kontrol ediliyor. ")
    tablo_olustur()
    conn.commit()
except Exception as exp:
    hata = """Veritabani ile alakali bir sorun var. Lutfen veritabani ismini, kullanici adini kontrol ediniz ve \
    veritabani servisinin calistigina emin olunuz.""" + str(exp)
    kayitci.error(hata)
    quit()
try:
    kayitci.info("Link dosyasi okunuyor.")
    link_dosyasi = open("linkler.txt",'r')
    linkler = link_dosyasi.read()
    linkler = linkler.split('\n')
    while 1:
        for link in linkler:
            tabloya_ekle(link)
        time.sleep(periyot)
    conn.close()
except IOError:
    hata =  """Link dosyasi bulunamadi. Lutfen linkler.txt dosyasinin varligindan, isminin "linker.txt" seklinde oldugundan ve icerigin,

    ****

    http://www.example1.com/rss
    http://www.example2.com/..../rss
    http://www.example2.com/..../feed

    ****

    seklinde oldugundan emin olun.
    """
    kayitci.error(hata)
    print "Link dosyasinda hata!"
    kapat()
except KeyboardInterrupt:
    kayitci.info("Cikis yapildi.")
    print "Bye"
    kapat()
except Exception as exp:
    kayitci.error("Ana fonksyonda bilinmeyen hata. %s" %(str(exp)))
    kapat()
